import smtplib
from socket import gaierror     #,ConnectionRefusedError
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
#from email.mime.MIMEImage import MIMEImage
from email.MIMEImage import MIMEImage
from bs4 import BeautifulSoup
from flask import request, jsonify

from templates import templates_dict
from flask import Flask

app = Flask(__name__)


port = 25 #2525   
smtp_server = "smtp.falconide.com"  #"smtp.mailtrap.io"

login = "bajajfinserv"    #"3bd8d0960d103c" #  login Mailtrap
password = "B@j#f1n*"    #"c55ed0f8622935" #  password Mailtrap


sender = "vcip@bajajfinserv.in"   #"from@example.com"
receiver = "peralisairam1997@gmail.com"   #"peralisairam1997@gmail.com"

##part1 = MIMEText(text, "plain")
#part2 = MIMEText(html, "html")
##message.attach(part1)
#message.attach(part2)




@app.route('/sendreq',methods=["POST"])
def send_req():
    data=request.get_json()
    sender = "vcip@bajajfinserv.in"
    receiver="vcip@bajajfinserv.in"
    message = MIMEMultipart("alternative")
    if type(data["receiver"]) is list:
        receiver=data["receiver"]
        message["To"] =", ".join(receiver)
    else:
        receiver=data["receiver"]
        message["To"] = data["receiver"]
    fp = open('BF-logo.jpg', 'rb')
    msgImage = MIMEImage(fp.read(),_subtype="jpg")
    fp.close()
    msgImage.add_header('Content-ID', '<image1>')
    msgImage.add_header('Content-Disposition', 'inline; filename=bajaj_fin.jpg')
    message.attach(msgImage)
    try:
        soup = BeautifulSoup(templates_dict[data["operation"]], "lxml")
    except Exception as e:
        return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    
    try:
        tag = soup.find(id="custName")
        tag.string.replace_with("Dear  {} ,".format(data['custName']))
    except Exception as e:
        return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    if data["operation"]=="vkyc_reminder":
        try:
            tag = soup.find(id="scheduleTime")
            tag.string.replace_with("You have an appointment scheduled at {scheduleTime}  with us to complete your video KYC to process loan Application.".format(scheduleTime=data['scheduleTime']))
        except Exception as e:
            return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    if data["operation"]=="vpd_reminder":
        try:
            tag = soup.find(id="scheduleTime")
            tag.string.replace_with("You have an appointment scheduled at {scheduleTime}  with us to complete your video discussion to process loan Application.".format(scheduleTime=data['scheduleTime']))
        except Exception as e:
            return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    try:
        tag = soup.find(id="bitlyLink")
        tag['href'] = data['bitlyLink']
        tag.string.replace_with(data['bitlyLink'])
    except Exception as e:
        return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    final_str=str(soup)
    part2 = MIMEText(final_str, "html")
    message.attach(part2)
    if data["operation"]=="vkyc_reminder" or data["operation"]=="vkyc":
        message["Subject"] = "Additional information requirement for Bajaj Loan Application"
        message["From"] = "vcip@bajajfinserv.in"
        sender = "vcip@bajajfinserv.in"
    else:
        message["From"] = "vpd@bajajfinserv.in"
        sender = "vpd@bajajfinserv.in"
        try:
            message["Subject"] = "Appointment for video call for Loan application number {}".format(data['applicationNumber'])
        except Exception as e:
            return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.login(login, password)
        server.sendmail(sender, receiver, message.as_string()    )
        print(str(sender))
        print(str(receiver))
        print(str(message["From"]))
        print(str(message["To"]))
        print(str(message["Subject"]))
        server.quit()
        print('Sent') 
    except Exception as e:
        return jsonify({"msg":str(e),"status_code": 400,"success": False}), 400
    return jsonify({"status_code": 200,"success": True}), 200 #'msg':final_str
        
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9998, debug=True)
    



