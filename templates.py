templates_dict={"vkyc_reminder":"""\
           <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-KYC reminder</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear < Customer name >,</p>
                   
                    <p id="scheduleTime" style="margin:0px;padding:10px 0px 5px 0px;">You have an appointment scheduled at hh:mm  with us to complete your video KYC to process loan Application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Please click <a id="bitlyLink" href=\"{}\" > Bitly </a> to initiate video KYC .</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Open the above link in:</p>
<ul>
  <li>For Android - Google Chrome 65+</li>
  <li>For iOS - Safari 11+</li>
  <li>For Desktop<ul><li>Windows 7+ -Google Chrome 65+</li><li>Mac OS - Safari 11+ / Google Chrome 65+</li></ul></li>
</ul>

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recepient.</p>									
                 									
                  </td>
              	</tr>
              	<tr>
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>""",
                 "vpd_reminder":"""\
                 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-KYC reminder</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear < Customer name >,</p>
                   
                    <p id="scheduleTime" style="margin:0px;padding:10px 0px 5px 0px;">You have an appointment scheduled at hh:mm  with us to complete your video discussion to process loan Application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Please click <a id="bitlyLink" href=\"{}\" > Bitly </a> to initiate video call with Bajaj executive and provide required details.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Open the above link in:</p>
<ul>
  <li>For Android - Google Chrome 65+</li>
  <li>For iOS - Safari 11+</li>
  <li>For Desktop<ul><li>Windows 7+ -Google Chrome 65+</li><li>Mac OS - Safari 11+ / Google Chrome 65+</li></ul></li>
</ul>
<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                      <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                      <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                   
                      <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recipient.</p>									
                 									
                  </td>
              	</tr>
              	<tr>
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>""",
                 "vkyc":"""\
                 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-KYC</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear  Customer_name ,</p>
                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">You are just a few steps away from disbursal for your loan application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Click <a id="bitlyLink" href=\"{}\" > Bitly </a> to initiate process of video KYC  and fix appointment.</p>
                    
                    <p style="margin:0px;padding:20px 0px 0px 0px;"><b>Please Note</b>: This requires you to verify your Aadhar number first and then fix appointment.

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recipient.</p>									
                  </td>
              	</tr>
              	<tr> 
                    
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>""",
                 "vpd":"""\
                 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="opacity: 1;" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video-PD</title>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <style type="text/css">
		img {
            max-width: 100%;
            }
		ol{
			padding-left: 15px;
    		margin: 0;
		}
		ol li{
		 	padding: 10px 0 5px 0px;	
		}
		ul li{
			padding: 10px 0 0px 0px;
			list-style-type:disc;	
		}
		.disc{
            border-top:1px solid #DFDEE2;
            border-bottom:1px solid #DFDEE2;
            padding: 10px 0px;
            width: 100%; 
		}
        @media only screen and (max-width: 767px) {
            .min-pad {
                padding-left: 10px !important;
				padding-right: 10px !important;
            }
		}
        @media only screen and (max-width: 480px) {
            .min-pad {
                padding-left: 5px !important;
				padding-right: 5px !important;
				font-size:12px;
            }
        }
    </style>
</head>
<body style="font-family: 'Arial'; font-size: 14px; margin: 0px; padding: 0px; color: #5a5a5a;">
   <p align="center" style="font-size: 12px;color: #54565b;margin: 15px 0px; "> If you are not able to view the content given below properly, please   <a href="#" target="_blank"> click here</a>
    </p>
    <table align="center" cellpadding="0" cellspacing="0" style="max-width:600px; min-width:280px; border:1px solid #DFDEE2;border-bottom:1px solid #DFDEE2; margin:auto;">
	<tbody>
		<tr>
			<td>
			<table class="min-pad" align="center" cellpadding="0" cellspacing="0" style=" padding:0px 20px 20px 20px; border-left:2px solid #0072bc; border-top:2px solid #0072bc;" width="100%">
			<tbody>
                <tr>
                	<td align="right" style="">
                        <a href="https://www.bajajfinserv.in" target="_blank"><img alt="Bajaj Finserv"  src="https://media.bajajfinserv.in/email/finance/mailer/2017/november/shl-loan-approval-text-mailer23-11-17/images/bfl-logo.jpg" /> </a>
                    </td>
                </tr>
                <tr>
                  <td align="left"  style="">
                    <p id="custName" style="margin:0px;padding:10px 0px 5px 0px;">Dear < Customer name >,</p>
                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">We need few additional details for processing of your loan application.</p>
<p style="margin:0px;padding:10px 0px 5px 0px;">Click <a id="bitlyLink" href="lol" > Bitly </a> to initiate video call with Bajaj executive and provide required details.</p>

<p style="margin:0px;padding:20px 0px 0px 0px;">It is our constant endeavour to ensure you have our best attention at all times.</P>
<p style="margin:0px;padding:20px 0px 0px 0px;">Have a great day!</P>

                    <p style="margin:0px;padding:20px 0px 0px 0px;">Warm Regards,</p>
                    <p style="margin:0px;padding:20px 0px 5px 0px;"><strong>BAJAJ FINANCE LIMITED</strong></p>	

                   
                    <p style="margin:0px;padding:10px 0px 5px 0px;">This is a system generated email. Please do not respond to this email. It will not reach to any recipient.</p>									
                 									
                  </td>
              	</tr>
              	<tr>
              		<td style="padding:10px 0 0 0;" align="left">
                    	<table class="disc" cellspacing="0" style="cellpadding=0" width="100%">
                        	<tr>
                            	<td style="">
                                	<p style="margin:0px; padding:0px 5px 0px 5px;line-height:12px; background-color:#fff; font-size: 10px; "> Disbursal of your loan request is at the sole discretion of Bajaj Finance Ltd and is governed by the terms and conditions applicable to your loan request.This is a system generated alert. We request you not to reply to this message. This e-mail is confidential and may also be privileged. If you are not the intended recipient, please notify us immediately; you should not copy or use it for any purpose, nor disclose its contents to any other person.</p>
                            	</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
			</tbody>
			</table>
			</td>
		</tr>
	</tbody>	
	</table>
</body>
</html>"""}
